## YAMM Client

__This code is under heavy development, and it's not ready for production yet, only for local development.__

This repository contains the code for the Front-End & BFF of the YAMM MVP.

* **Front-End**: [React](https://reactjs.org/) framework and [Webpack](https://webpack.js.org/) as assets manager.
* **BFF**: [Backend-For-Front](http://samnewman.io/patterns/architectural/bff/), provides all the required API endpoints to the Front-End, with the specific formats of each functionality that will be used by the customers.

### Authentication Pattern

In the MVP, there isn't any kind of authentication used by the Front-End to access BFF's provided routes.

### Business Rules

All the rules and logic related to the basic functionalities (expenses, incomes and transfers register) will be contained in API's located at the YAMM Services repositories. The BFF will interact with this services, and knows how to merge the necessary information.

The BFF will not contains any business rule, having only the responsibility of call one or more services and orchestrate their returns.

---

## Dependencies

To run the project in your local dev enviroment, you will need:

* [RVM](https://rvm.io/)
* [Ruby 2.5.0](https://www.ruby-lang.org/pt/)
* [Rails 5.1.6](http://rubyonrails.org/)
* [YARD](https://yardoc.org/)
* [Bundler](http://bundler.io/)
* [Nodejs](https://nodejs.org/en/)
* [Yarn](https://yarnpkg.com/pt-BR/)

All the instructions to install these tools can be found on file [TOOLS_INSTALL](/TOOLS_INSTALL.md).

---

## Running the YAMM Client

After dependencies installation, clone the repository and execute the following commands:

```bash
$ git clone git@bitbucket.org:yet-another-money-manager/yamm_client.git
$ cd yamm_client/
$ ./bin/setup && ./bin/server
```

It's necessary that you have [YAMM Transaction Service](https://bitbucket.org/yet-another-money-manager/yamm_transactionservice) running at [localhost:3003](localhost:3003).
---

## Tests, Code Quality & Style

This project relies on some tools to ensure the appropriate test coverage, besides the code quality and cleanessny.

It's important to stick with this rules, to maintain an high code quality and the app bugs-free

### Tests

Tests creation and execution follow [RSpec patterns](http://rspec.info/). To run them, execute the following command:

```bash
$ bundle exec rspec
```

### Coverage

After each Test Suite execution, an report showing the overall coverage is created by SimpleCov gem. This report can be found at the folder **coverage/**. Just open the file **rcov/index.html** on your browser.

### Code Style

The code style checks are made with the rules configured on [Rubocop](http://batsov.com/rubocop/), just run the command:

```bash
$ rubocop
```

If some offense is found, its possible to execute the command `rubocop -a` to make an auto-correction of some errors/warnings. If it still have some problems, they will need to be corrected manually.

### Code Quality

The general code quality verification is made by the rules of [Rubycritic](https://github.com/whitesmith/rubycritic/) gem. To generate an assessment, just run the command below:

```bash
$ rubycritic app lib
```

### Code Documentation

To view all the code documentation, run the following command:

```bash
$ yard server
```

Go to `http://localhost:8808/` address to browse the generated docs.

---

## Credits

@danilo_barion & @wylkon

---

## Contributions

1. Clone the repo!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Send your branch to the repository: `git push origin my-new-feature`
5. Open an Pull Request :D
