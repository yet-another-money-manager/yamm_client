# frozen_string_literal: true

# Model that represents the object returned by one YAMM Service
class ClassName
  include ModelBase

  attr_accessor :id
end
