# frozen_string_literal: true

# Class responsible for retrieve data from the service
class ClassNameRequest < Apis::Request
  attr_reader :attributes

  def initialize(attributes)
    @attributes = attributes
  end

  def host
    Rails.application.config.x.yamm_services['class_name_service']['host']
  end

  def service_path
    '/v1/service_path'
  end

  def as_object(body, _response)
    body[:data].map { |item| ClassNameModel.new(item) }
  end

  def parameters
    params = { attributes: attributes }
  end
end
