# frozen_string_literal: true

# Serializer that exposes information of the model
class ClassNameSerializer < ActiveModel::Serializer
  include NullAttributesRemover

  attributes :id
end
