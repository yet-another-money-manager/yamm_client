# frozen_string_literal: true

# Controller responsible for handle service operations
class V1::ClassNameController < V1::BaseController
  def index
    requester = request_for(ClassNameRequest, params)
    render_requester(requester, serializer: ClassNameSerializer)
  end

  private

  def request_for(requester, parameters)
    requests = Apis::Requests.new key: requester.new(parameters)
    requests.run
    requests[:key]
  end
end
