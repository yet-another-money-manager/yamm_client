# frozen_string_literal: true

require 'digest/md5'

##
# Controller para entrega da interface Web do projeto
class AppController < ApplicationController
  layout 'application'

  rescue_from ArgumentError do |error|
    redirect_to "/?fail=#{error.message}"
  end

  def index
    redirect_to "/fake-login" if cookies[:access_token].nil?
  end

  private

  def user_id
    @user.id
  end

  def idioma_navegacao
    cookies[:IntegracaoLoginIdioma] || '_ptBR'
  end

  def cookie(value)
    { value: value, domain: nil, expires: 1.day.from_now }
  end

  def md5_user
    salt = 'yamm-user-integration:'
    Digest::MD5.hexdigest("#{salt}#{user_id}")
  end

  def logout?
    params[:service] == 'end_session'
  end
end
