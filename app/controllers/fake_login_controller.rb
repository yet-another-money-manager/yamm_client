# frozen_string_literal: true

# Simula login realizado no ASP
class FakeLoginController < ApplicationController
  def create
    cookies[:access_token] = "TOKEN_PARA:#{params[:cliente_id]}-#{params[:funcionario_id]}"
    session[:cliente_id] = params[:cliente_id]
    session[:funcionario_id] = params[:funcionario_id]
    redirect_to '/'
  end
end
