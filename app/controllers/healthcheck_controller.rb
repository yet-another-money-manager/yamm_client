# frozen_string_literal: true

##
# Controller para teste se sistema esta disponivel
class HealthcheckController < ApplicationController
  def index
    render json: {
      client_version: 'v1',
      tag_version: current_tag_version,
      yamm_services: check_yamm_services
    }, status: 200
  end

  private

  def current_tag_version
    File.read(File.join(Rails.root, 'version.current')).strip
  end

  def check_yamm_services
    [ { transaction_service: request_for(YAMMTransactionServiceHealthcheckRequest).object } ]
  end

  private

  def request_for(requester)
    requests = Apis::Requests.new(key: requester.new)
    requests.run
    requests[:key]
  end
end
