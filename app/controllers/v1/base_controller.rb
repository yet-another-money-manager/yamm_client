# frozen_string_literal: true

# Acoes basicas que serao usadas em todas as chamados para os controllers da aplicacao
class V1::BaseController < ApplicationController
  attr_reader :user

  before_action :retrieve_user, :validate_access

  private

  def user_id_from_header
    return session[:cliente_id] if (Rails.env.development? || Rails.env.test?) && session[:cliente_id]
    request.headers['HTTP_X_USER_ID']
  end

  def retrieve_user
    requests = Apis::Requests.new(
      user: UserRequest.new(user_id_from_header.to_i)
    )
    requests.run
    @user = requests[:user].object
  end

  # Valida se o HEADER possui as informacoes necessarias
  def validate_access
    render json: { error: 'Unauthorized' }, status: 401 unless @user
  end
end
