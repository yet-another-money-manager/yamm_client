# frozen_string_literal: true

# Controller responsible for handle expenses operations
class V1::ExpensesController < V1::BaseController
  def index
    requester = request_for(ExpenseListRequest)
    render_requester(requester, serializer: ExpenseListSerializer)
  end

  private

  def request_for(requester)
    requests = Apis::Requests.new key: requester.new(@user.id)
    requests.run
    requests[:key]
  end
end
