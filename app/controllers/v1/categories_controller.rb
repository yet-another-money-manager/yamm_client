# frozen_string_literal: true

# Controller responsible for handle categories operations
class V1::CategoriesController < V1::BaseController
  def index
    requester = request_for(CategoryListRequest)
    render_requester(requester, serializer: CategoryListSerializer)
  end

  private

  def request_for(requester)
    requests = Apis::Requests.new key: requester.new(@user.id)
    requests.run
    requests[:key]
  end
end
