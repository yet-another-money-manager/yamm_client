# frozen_string_literal: true

class ApplicationController < ActionController::Base
  # protect_from_forgery with: :exception
  layout false

  def render_requester(requester, options = {})
    return render({ json: requester.object }.merge(options)) if requester.success?
    render json: requester.http_body, status: requester.status
  end
end
