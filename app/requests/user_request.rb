# frozen_string_literal: true

# Class responsible for retrieve the user of the app
class UserRequest < Apis::Request
  attr_reader :user_id

  def initialize(user_id)
    @user_id = user_id
  end

  def host
    Rails.application.config.x.yamm_services['transaction_service']['host']
  end

  def service_path
    "/v1/users"
  end

  def as_object(body, _response)
    User.new(body[:users].select { |user| user[:id] == user_id }.first)
  end

  def parameters
    params = { user_id: user_id }
  end
end
