# frozen_string_literal: true

# Class responsible for retrieve a list of expenses of the specified user
class ExpenseListRequest < Apis::Request
  attr_reader :customer_id

  def initialize(customer_id)
    @customer_id = customer_id
  end

  def host
    Rails.application.config.x.yamm_services['transaction_service']['host']
  end

  def service_path
    "/v1/expenses"
  end

  def as_object(body, _response)
    body[:expenses].map { |expense| Expense.new(expense) }
  end

  def parameters
    params = { customer_id: customer_id }
  end
end
