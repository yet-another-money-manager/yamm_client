# frozen_string_literal: true

# Class responsible for retrieve a list of categories of the specified user
class CategoryListRequest < Apis::Request
  attr_reader :user_id

  def initialize(user_id)
    @user_id = user_id
  end

  def host
    Rails.application.config.x.yamm_services['transaction_service']['host']
  end

  def service_path
    "/v1/categories"
  end

  def as_object(body, _response)
    body[:categories].map { |category| Category.new(category) }
  end

  def parameters
    params = { user_id: user_id }
  end
end
