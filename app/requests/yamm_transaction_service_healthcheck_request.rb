# frozen_string_literal: true

# Class responsible for retrieve the user of the app
class YAMMTransactionServiceHealthcheckRequest < Apis::Request
  def host
    Rails.application.config.x.yamm_services['transaction_service']['host']
  end

  def service_path
    '/healthcheck'
  end

  def as_object(body, _response)
    body
  end
end
