# frozen_string_literal: true

# Class responsible for retrieve data from the service
class AccountListRequest < Apis::Request
  attr_reader :attributes

  def initialize(attributes)
    @attributes = attributes
  end

  def host
    Rails.application.config.x.yamm_services['transaction_service']['host']
  end

  def service_path
    '/v1/accounts'
  end

  def as_object(body, _response)
    body[:accounts].map { |item| Account.new(item) }
  end

  def parameters
    params = { attributes: attributes }
  end
end
