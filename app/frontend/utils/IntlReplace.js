export function intlReplace(string) {
  return `${string.toUpperCase().replace(/[.-]/g, '_')}`;
}
