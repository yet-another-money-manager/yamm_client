import superagent from 'superagent';
import Cookie from 'js-cookie';
import { forEach } from 'lodash';

const methods = ['get', 'post', 'put', 'patch', 'del'];
const LOCALES = {
  _enUS: 'en',
  _ptBR: 'pt-BR',
  _esALT: 'es',
};

const defaultRequestOptions = {
  headers: {
    'Content-Type': 'application/json',
    authorization: `Bearer ${Cookie.get('access_token')}`,
    'Accept-Language': `${LOCALES[Cookie.get('IntegracaoLoginIdioma')]}`,
    'Access-Control-Allow-Origin': process.env.ACCESS_CONTROL,
  },
};

export default class ApiClient {
  constructor() {
    forEach(
      methods,
      method =>
        (this[method] = (path, options = {}, domain = process.env.YAMM_URL) =>
          new Promise((resolve, reject) => {
            const request = superagent[method](`${domain}${path}`);

            request.set(defaultRequestOptions.headers);

            if (options && options.params) {
              request.query(options.params);
            }

            if (options && options.data) {
              request.send(options && options.data);
            }

            request.end((err, { body } = {}) => (err ? reject(body || err) : resolve(body)));
          }))
    );
  }
  /*
   * There's a V8 bug where, when using Babel, exporting classes with only
   * constructors sometimes fails. Until it's patched, this is a solution to
   * "ApiClient is not defined" from issue #14.
   * https://github.com/erikras/react-redux-universal-hot-example/issues/14
   *
   * Relevant Babel bug (but they claim it's V8): https://phabricator.babeljs.io/T2455
   *
   * Remove it at your own risk.
   */
  empty() {}
}
