import clientMiddleware from './middleware/clientMiddleware';
import thunk from 'redux-thunk';
import { createStore, applyMiddleware, compose } from 'redux';
import { createLogger } from 'redux-logger';
import rootReducer from 'modules';

const logger = createLogger();

export default function configureStore(initialState, client) {
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

  const getMiddleware = () =>
    process.env.NODE_ENV !== 'production'
      ? [clientMiddleware(client), thunk, logger]
      : [clientMiddleware(client), thunk];

  const createStoreWithMiddleware = composeEnhancers(applyMiddleware(...getMiddleware()))(createStore);
  const store = createStoreWithMiddleware(rootReducer, initialState);

  if (module.hot) {
    // Enable webpack hot module replacement for reducers
    module.hot.accept('./modules/index', () => {
      console.info('🍭 Reloading Reducers'); // eslint-disable-line no-console
      const nextReducer = require('./modules/index').default;
      store.replaceReducer(nextReducer);
    });
  }
  return store;
}
