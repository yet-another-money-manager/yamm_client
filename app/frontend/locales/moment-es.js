//! moment.js locale configuration
//! locale : Portuguese (Brazil) [pt-br]
//! author : Caio Ribeiro Pereira : https://github.com/caio-ribeiro-pereira

(function(global, factory) {
  typeof exports === 'object' &&
  typeof module !== 'undefined' &&
  typeof require === 'function'
    ? factory(require('moment'))
    : typeof define === 'function' && define.amd
      ? define(['moment'], factory)
      : factory(global.moment);
})(this, function(moment) {
  'use strict';

  var es = moment.defineLocale('es', {
    months: 'enero_febrero_marzo_abril_mayo_junio_julio_agosto_septiembre_octubre_noviembre_diciembre'.split(
      '_'
    ),
    monthsShort: 'ene_feb_mar_abr_may_jun_jul_ago_sep_oct_nov_dec'.split('_'),
    weekdays: 'Domingo_Lunes_Martes_Miércoles_Jueves_Viernes_Sabado'.split(
      '_'
    ),
    weekdaysShort: 'Dom_Lun_Mar_Mie_Jue_Vie_Sab'.split('_'),
    weekdaysMin: 'Do_2ª_3ª_4ª_5ª_6ª_Sa'.split('_'),
    weekdaysParseExact: true,
    longDateFormat: {
      LT: 'HH:mm',
      LTS: 'HH:mm:ss',
      L: 'DD/MM/YYYY',
      LL: 'D [de] MMMM [de] YYYY',
      LLL: 'D [de] MMMM [de] YYYY [a las] HH:mm',
      LLLL: 'dddd, D [de] MMMM [de] YYYY [a las] HH:mm',
    },
    calendar: {
      sameDay: '[Hoy a las] LT',
      nextDay: '[Mañana a las] LT',
      nextWeek: 'dddd [a las] LT',
      lastDay: '[Ayer a las] LT',
      lastWeek: function() {
        return this.day() === 0 || this.day() === 6
          ? '[Ultimo] dddd [a las] LT' // Saturday + Sunday
          : '[Ultima] dddd [a las] LT'; // Monday - Friday
      },
      sameElse: 'L',
    },
    relativeTime: {
      future: 'en %s',
      past: 'hace %s',
      s: '%d segundos',
      ss: '%d segundos',
      m: '%d minuto',
      mm: '%d minutos',
      h: '%d hora',
      hh: '%d horas',
      d: '%d día',
      dd: '%d días',
      M: '%d mes',
      MM: '%d meses',
      y: '%d año',
      yy: '%d años',
    },
    dayOfMonthOrdinalParse: /\d{1,2}º/,
    ordinal: '%dº',
  });

  return es;
});
