import { forEach } from 'lodash';
const req = require.context('.', true, /\.jsx$/);

forEach(req.keys(), key => {
  const filename = key.match(/[a-zA-Z]+.jsx$/)[0];
  const componentName = filename.split('.')[0];
  console.log('filename: ' + filename)
  console.log('componentName: ' + componentName)
  console.log('key: ' + key)
  module.exports[componentName] = req(key).default;
});
