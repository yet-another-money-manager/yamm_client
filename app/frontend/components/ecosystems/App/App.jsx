import propTypes from 'prop-types';
import intl from 'react-intl-universal';
import React, { Component } from 'react';
import { TransitionGroup } from 'react-transition-group';
import { Switch, Route } from 'react-router-dom';
import Moment from 'moment';
import Cookie from 'js-cookie';
import {
  NotFoundPage,
  HomePage
} from 'components';
import 'babel-polyfill';

// locale data
const LOCALES = {
  _enUS: 'en-US',
  _ptBR: 'pt-BR',
  _esALT: 'es',
};

Cookie.set(
  'IntegracaoLoginIdioma',
  Cookie.get('IntegracaoLoginIdioma', { domain: process.env.DOMAIN_COOKIE }) || '_ptBR',
  {
    domain: process.env.DOMAIN_COOKIE,
  }
);

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      language: Cookie.get('IntegracaoLoginIdioma'),
      currentLocale: Cookie.get('IntegracaoLoginIdioma'),
    };

    this.changeLanguage = this.changeLanguage.bind(this);
    this.state.currentLocale = LOCALES[this.state.language];

    intl.init({
      currentLocale: this.state.currentLocale,
      locales: {
        [this.state.currentLocale]: require(`locales/${this.state.currentLocale}.json`),
      },
    });

    Moment.locale(this.state.currentLocale);
  }

  changeLanguage(value) {
    this.setState(
      {
        language: value,
        currentLocale: LOCALES[value],
      },
      () => {
        Cookie.set('IntegracaoLoginIdioma', value, { domain: process.env.DOMAIN_COOKIE });
        intl.init({
          currentLocale: LOCALES[value],
          locales: {
            [LOCALES[value]]: require(`locales/${LOCALES[value]}.json`),
          },
        });
        // location.reload();
      }
    );
  }

  render() {
    const { location } = this.props;
    return (
      <main className="vg-main">
        <TransitionGroup>
          <div className="vg-fix-transition">
            <Switch location={location}>
              <Route exact path="/" component={HomePage} />
              <Route path="**" component={NotFoundPage} />
            </Switch>
          </div>
        </TransitionGroup>
      </main>
    );
  }
}

App.propTypes = {
  location: propTypes.object.isRequired,
};

export default App;
