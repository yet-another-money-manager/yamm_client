import React from 'react';
import intl from 'react-intl-universal';
import Moment from 'moment';
import { SelectiveProcessList, Intro, FastLink, Graphs } from 'components';

export default function HomePage() {
  return (
    <div className="vg-home-page">
      <Intro type="painel" path="painel" />
      <div className="vg-container">
        <section className="vg-row">
          <div className="vg-col-md-12 vg-col-xs-12">
            <FastLink type="painel" path="painel" />
          </div>
        </section>
        <section className="vg-row">
          <div className="vg-col-md-12 vg-col-xs-12">
            <Graphs />
          </div>
        </section>
        <section className="vg-row">
          <div className="vg-col-md-12 vg-col-xs-12">
            <h1 className="vg-title-1 vg-text-center">{intl.get('SERVICOS.SUAS_VAGAS')}</h1>
            <SelectiveProcessList
              type="home"
              path={`home?apenas_do_funcionario=true&data_criacao_depois_de=${Moment()
                .subtract(4, 'month')
                .format('YYYY-MM-DD')}`}
            />
          </div>
        </section>
      </div>
    </div>
  );
}
