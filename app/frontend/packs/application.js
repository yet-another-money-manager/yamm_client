import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
// import configureStore from 'store/configureStore';
import ApiClient from 'utils/ApiClient';
import { BrowserRouter, Route } from 'react-router-dom';
// import { Provider } from 'react-redux';
import { App } from 'components';

const client = new ApiClient();
// const ReduxStore = configureStore(window.REDUX_INITIAL_DATA, client);
const rootElement = document.getElementById('root');

ReactDOM.render(
  // <Provider store={ReduxStore}>
    <BrowserRouter>
      <Route path="/" component={App} />
    </BrowserRouter>,
  //</Provider>,
  rootElement
);
