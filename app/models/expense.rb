# frozen_string_literal: true

# Model that represents the object returned by one YAMM Service
class Expense
  include ModelBase

  attr_accessor :id,
                :name,
                :amount,
                :date,
                :category_id,
                :account_id,
                :transaction_status_id
end
