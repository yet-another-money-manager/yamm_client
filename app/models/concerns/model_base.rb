# frozen_string_literal: true

# Modulo responsavel por possibilitar a utilizacao de um construtor
# com passagem de atributos e retira apenas os atributos indicados no
# attr_accessor
module ModelBase
  include ActiveModel::ForbiddenAttributesProtection

  alias read_attribute_for_serialization send

  def initialize(attributes = {})
    assign_attributes(attributes) if attributes
    super()
    after_initialize
  end

  def assign_attributes(new_attributes)
    unless new_attributes.respond_to?(:stringify_keys)
      raise ArgumentError, 'When assigning attributes, you must pass a hash as an argument.'
    end
    return if new_attributes.nil? || new_attributes.empty?

    _assign_attributes(sanitize_for_mass_assignment(new_attributes.stringify_keys))
  end

  def after_initialize; end

  private

  def _assign_attributes(attributes)
    attributes.each { |key, value| _assign_attribute(key, value) }
  end

  def _assign_attribute(key, value)
    public_send("#{key}=", value) if respond_to?("#{key}=")
  end
end
