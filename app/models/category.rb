# frozen_string_literal: true

# Model that represents the object returned by one YAMM Service
class Category
  include ModelBase

  attr_accessor :id,
                :name,
                :description,
                :category_type
end
