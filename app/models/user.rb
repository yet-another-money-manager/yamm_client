# frozen_string_literal: true

# Model that represents the object returned by one YAMM Service
class User
  include ModelBase

  attr_accessor :id,
                :name
end
