# frozen_string_literal: true

# Module responsible for remove null attributes at the JSON response
module NullAttributesRemover
  def serializable_hash(adapter_options = nil,
                        options = {},
                        adapter_instance = self.class.serialization_adapter_instance)
    hash = super
    hash.tap { |h| h.each { |key, value| hash.delete(key) if value.nil? } }
  end
end
