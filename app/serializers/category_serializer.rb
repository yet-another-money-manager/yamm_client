# frozen_string_literal: true

# Serializer that exposes information of the Category model
class CategorySerializer < ActiveModel::Serializer
  include NullAttributesRemover

  attributes :id,
             :name,
             :description,
             :category_type
end
