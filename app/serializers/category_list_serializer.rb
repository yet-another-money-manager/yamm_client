# frozen_string_literal: true

# Serializer that exposes information of the Category model
class CategoryListSerializer < ActiveModel::Serializer
  include NullAttributesRemover

  attributes :categories

  def categories
    object.map { |item| CategorySerializer.new(item) }
  end
end
