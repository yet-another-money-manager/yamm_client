# frozen_string_literal: true

# Serializer that exposes information of the Expense model
class ExpenseSerializer < ActiveModel::Serializer
  include NullAttributesRemover

  attributes :id,
             :name,
             :amount,
             :date,
             :category_id,
             :account_id,
             :transaction_status_id
end
