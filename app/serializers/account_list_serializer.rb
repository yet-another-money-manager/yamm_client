# frozen_string_literal: true

# Serializer that exposes information of the model
class AccountListSerializer < ActiveModel::Serializer
  include NullAttributesRemover

  attributes :accounts

  def accounts
    object.map { |item| AccountSerializer.new(item) }
  end
end
