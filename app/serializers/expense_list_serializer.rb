# frozen_string_literal: true

# Serializer that exposes information of the Expense model
class ExpenseListSerializer < ActiveModel::Serializer
  include NullAttributesRemover

  attributes :expenses

  def expenses
    object.map { |item| ExpenseSerializer.new(item) }
  end
end
