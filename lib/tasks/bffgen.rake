namespace :bffgen do
  desc 'Create Model, Request, Controller and serializer classes for BFF, using specified templates contained on specific templates_folder'
  task :resource, [:name, :templates_folder] do |task, args|
    validate_params(args[:name])
    %w[model request controller serializer].each do |type|
      folder = args[:templates_folder] || 'app/bff_templates'
      src = source_file(type, folder)
      name = type == 'model' ? args[:name] : "#{args[:name]}_#{type}"
      folder = type == 'controller' ? "#{type}s/v1" : "#{type}s"
      dest = destination_file(folder, name)
      FileUtils.cp(src, dest)
    end
  end

  private

  def validate_params(name)
    raise ArgumentError, 'name is mandatory!' if name.nil?
  end

  def source_file(type, folder)
    File.join(Rails.root, folder, "#{type}.rb")
  end

  def destination_file(folder, name)
    File.join(Rails.root, 'app', "#{folder}", "#{name.underscore}.rb")
  end
end
