# frozen_string_literal: true

require_relative '../refinements/string_extensions'

# Module indicador de APIs externas, tratamento comum para requisicoes pelo Typhoeus
module Apis
  using StringExtensions

  # Classe que faz o tratamento de resposta das requisicoes.
  # Deve ser estendida com as informacoes extras de service_uri e as_object
  class Request
    attr_reader :object, :response, :healthcheck
    HTTP_UNPROCESSABLE_ENTITY = 422

    def host
      raise MethodMissingError
    end

    def service_path
      raise MethodMissingError
    end

    def as_object(_body, _response)
      raise MethodMissingError
    end

    def parameters
      nil
    end

    def request_body
      nil
    end

    def http_body
      body = response.body

      return response_error if body.empty?
      return response_invalid_format unless body.valid_json?

      Oj.load(body, symbol_keys: true)
    end

    def response_error
      code = status

      case code
      when 404
        Oj.load({ error: 'Informação não encontrada', code: code }.to_json, symbol_keys: true)
      when 500
        Oj.load({ error: 'Erro interno da API', code: code }.to_json, symbol_keys: true)
      else
        Oj.load({ error: 'Erro não esperado', code: code }.to_json, symbol_keys: true)
      end
    end

    def response_invalid_format
      Oj.load({ error: 'Invalid JSON format', status: HTTP_UNPROCESSABLE_ENTITY }.to_json)
    end

    def status
      response.code
    end

    def success?
      response.success?
    end

    def method
      :get
    end

    def headers
      {}
    end

    def service_uri
      "#{host}#{service_path}"
    end

    def request
      Typhoeus::Request
        .new(service_uri, method: method, params: parameters, body: request_body, headers: http_header).tap do |req|
          handle_request(req)
        end
    end

    def handle_request(request)
      request.on_complete do |response|
        @response = response
        if response.success?
          @object = as_object(http_body, response)
          # elsif response.timed_out?
          #   Rails.logger.info('got a time out')
          #   # Poderiamos tentar requisitar mais uma vez
        else
          Rails.logger.warn("HTTP request failed: #{response.code}")
        end
      end
    end

    def request_healthcheck
      request = Typhoeus::Request.new("#{host}/healthcheck", params: parameters)
      @healthcheck = {}
      request.on_complete do |response|
        healthcheck[:status] = response.code
        healthcheck[:time] = response.total_time
      end
      request
    end

    private

    def http_header
      { 'User-Agent' => 'BFF e-parthenon - e-Partner Service' }.merge(headers || {})
    end
  end

  # Classe que representa uma requisicao nula, para nao precisar tratar valores nulos
  class NilRequest < Request
    def service_uri
      raise 'Invalid request'
    end

    def as_object(_body, _response)
      nil
    end

    def status
      404
    end

    def success?
      false
    end
  end

  # Classe encapsula todas as requisicoes do Typhoeus e executa conforme a chamada
  class Requests
    attr_reader :requests

    def initialize(requests = {})
      @requests = requests
    end

    def []=(key, requester)
      requests[key] = requester
    end

    def [](key)
      requests.fetch(key, NilRequest.new)
    end

    def run
      return self if requests.empty?
      # TODO: Tratar uma requisicao de apenas um objeto com request direto
      # sem a criacao de uma Hydra
      hydra = Typhoeus::Hydra.hydra
      requests.each_pair { |_key, req| hydra.queue(req.request) }
      hydra.run
      self
    end

    def run_healthcheck
      return :no_services if requests.empty?
      hydra = Typhoeus::Hydra.hydra
      requests.each_pair { |_key, req| hydra.queue(req.request_healthcheck) }
      hydra.run
      requests.map { |key, req| [key, req.healthcheck] }.to_h
    end
  end
end
