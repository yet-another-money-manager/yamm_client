# frozen_string_literal: true

# Module para inclusao de metodos na classe String de forma mais segura (com uso de refinements)
module StringExtensions
  refine String do
    # Verifica se a string possui um formato json valido.
    # Se valido, retorna true; caso contrario false.
    def valid_json?
      JSON.parse(self)
      true
    rescue JSON::ParserError
      false
    end
  end
end
