# frozen_string_literal: true

require 'encryptor'
require 'base64'

# Classe responsavel por criptografar e encodar (base64) dados sensiveis
#
# O algoritmo padrao utilizado para criptografar os dados eh o aes-256-gcm
class DataEncryptor
  class << self
    # Gostaria de colocar estes dados em um arquivo de config
    # mas, por estas strings nao serem UTF-8, da problema na leitura de um arquivo YAML
    #
    # Estas configuracoes sao as mesmas utilizadas no projeto Grande Irmao
    ENCRYPTOR_CONFIG = {
      key: "\x9A\x03\xEFT\x13b\xDC\x9AR\xBF\xB8\xF4\r\xF7\xAA<i\xBD\xD2\xA1\xA9\\\x8CXa[]\xB0\xFD8\xAF)",
      iv: "\xCD~\x17\xD8F\xC5\xEE\x9F\xEF\xF0\xDC\xF7",
      salt: 'vivo e funcionando'
    }.freeze

    def encrypt(mensagem)
      Encryptor.encrypt(mensagem, ENCRYPTOR_CONFIG)
    end

    def decrypt(encrypted)
      Encryptor.decrypt(encrypted, ENCRYPTOR_CONFIG)
    end

    def urlsafe_encode64(msg)
      Base64.urlsafe_encode64(msg)
    end

    def urlsafe_decode64(base64_msg)
      Base64.urlsafe_decode64(base64_msg)
    end
  end
end
