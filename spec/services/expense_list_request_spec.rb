# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ExpenseListRequest, type: :service do
  let(:customer_id) { 1 }

  subject do
    Apis::Requests.new(key: ExpenseListRequest.new(
      customer_id: customer_id
    )).run
  end

  context 'when there is expenses' do
    it 'returns with HTTP status 200 and with items in the expense list' do
      VCR.use_cassette('expense-list-with-items-index') do
        requests = subject
        object = requests[:key].object
        expect(requests[:key].status).to eql 200
        expect(object.expenses).not_to be_empty
      end
    end
  end

  context 'when there is not expenses' do
    it 'returns with HTTP status 200 and without any item in the expense list' do
      VCR.use_cassette('expense-list-empty-index') do
        requests = subject
        object = requests[:key].object
        expect(requests[:key].status).to eql 200
        expect(object.expenses).to be_empty
      end
    end
  end
end
