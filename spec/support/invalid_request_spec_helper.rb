# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'invalid request' do
  let(:code_invalid_request) { 412 }

  it 'returns HTTP status 412' do
    expect(response).to have_http_status code_invalid_request
  end

  it 'shows the correct error' do
    expect(json_response).to eql retorno_erro_validacao
  end
end
