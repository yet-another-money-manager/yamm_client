# frozen_string_literal: true

module Service
end

module Service::JsonHelpers
  def json_keys(response_object)
    Oj.load(response_object.to_json, symbol_keys: true).keys
  end
end
