# frozen_string_literal: true

module ModelHelper
end

module ModelHelper::Fixtures
  def hash_fixture(file)
    YAML.safe_load(File.read(File.join(Rails.root, 'spec', 'fixtures', "#{file}.rb")))
  end
end

module ModelHelper::Permissions
  def mock_permission(klass, metodo, retorno)
    allow_any_instance_of(klass).to receive(metodo).and_return(retorno)
  end
end
