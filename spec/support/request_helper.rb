# frozen_string_literal: true

module Request
end

module Request::JsonHelpers
  def json_response
    @json_response ||= Oj.load(response.body, symbol_keys: true)
  end

  def reset_json_response
    @json_response = nil
  end
end

module Request::ExpectSuccess
  def expect_success(response)
    expect(response).to be_success
    expect(response).to have_http_status(200)
  end
end

module Request::ExpectError
  def expect_unauthorized(response)
    expect(response).not_to be_success
    expect(response).to have_http_status(401)
  end

  def expect_invalid_params(response)
    expect(response).not_to be_success
    expect(response).to have_http_status(412)
  end
end

module Request::MockRequest
  def mock_request(key, object, status, http_body = nil)
    request = Struct.new(:object, :status, :http_body, :success?)
    allow_any_instance_of(Apis::Requests).to receive(:run)
    allow_any_instance_of(Apis::Requests).to receive(:[])
      .with(key).and_return(request.new(object, status, http_body || object, status < 400))
  end
end

module Request::SimulateAuthorization
  include Request::MockRequest

  def simulate_authorization(empresa, funcionario)
    mock_request(:cliente, empresa, 200)
    mock_request(:funcionario, funcionario, 200)
  end
end
