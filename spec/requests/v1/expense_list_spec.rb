# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Expense List', type: :request do
  let(:customer_id) { 1 }

  let(:expected_return) do
    {
      expenses:
      [
        {
          id: 1
          category: 'Foods',
          amount: 10.50,
          date: '2018-08-30'
        },
        {
          id: 2
          category: 'Car',
          amount: 800.00,
          date: '2018-08-31'
        }
      ]
    }
  end

  before do
    #simulate_authorization(Cliente.new(id: empresa_id),
    #                       Funcionario.new(id: funcionario_id, permissoes: {}))
    mock_request(:key, expected_return, 200)
  end

  describe 'GET /v1/expenses' do
    subject do
      get v1_expenses_path(customer_id)
    end

    it 'returns HTTP status 200' do
      subject
      expect(response).to be_success
    end

    it 'returns in the correct format' do
      subject
      expect(json_response).to include(expected_return)
    end
  end
end
