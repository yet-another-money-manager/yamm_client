# frozen_string_literal: true

Rails.application.config.x.yamm_services = Rails.application.config_for(:yamm_services)
