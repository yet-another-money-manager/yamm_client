# frozen_string_literal: true

# Definicoes de localidades entre ASP e usado pelo sistema HTTP
class Locales
  LOCALES_HASH = { 'en' => '_enUS', 'pt-BR' => '_ptBR', 'es' => '_esALT' }
                 .freeze

  def self.from_asp(locale_from_asp)
    LOCALES_HASH.key(locale_from_asp) || 'pt-BR'
  end

  def self.to_asp(locale)
    LOCALES_HASH.fetch(locale) { '_ptBR' }
  end
end
