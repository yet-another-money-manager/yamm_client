const { environment } = require('@rails/webpacker');
const webpack = require('webpack');
const dotenv = require('dotenv');
const dotenvFiles = [`env.${process.env.NODE_ENV}.local`, 'env.local'];

dotenvFiles.forEach(dotenvFile => {
  dotenv.config({ path: `config/${dotenvFile}`, silent: true });
});

environment.plugins.append('Environment', new webpack.EnvironmentPlugin(JSON.parse(JSON.stringify(process.env))));

module.exports = environment;
