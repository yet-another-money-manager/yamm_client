process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const eslintFormatter = require('react-dev-utils/eslintFormatter');
const environment = require('./environment');

environment.loaders.append('eslint', {
  test: /\.(js|jsx|mjs)$/,
  enforce: 'pre',
  use: [
    {
      loader: require.resolve('eslint-loader'),
      options: {
        formatter: eslintFormatter,
        eslintPath: require.resolve('eslint'),
      },
    },
  ],
});

module.exports = environment.toWebpackConfig();
