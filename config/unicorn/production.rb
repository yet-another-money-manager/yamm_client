# frozen_string_literal: true

app_path = '/opt/vagas/e_parthenon'

# Set unicorn options
worker_processes 3
preload_app true
timeout 5
listen '/tmp/e_parthenon.socket', backlog: 64

# Spawn unicorn master worker for user apps (group: apps)
user 'e_parthenon'

# Fill path to your app
working_directory app_path

# Should be 'production' by default, otherwise use other env
rails_env = ENV['RAILS_ENV'] || 'production'

# Log everything to one file
stderr_path '/var/log/e_parthenon/errors.log'
stdout_path '/var/log/e_parthenon/access.log'

# Set master PID location
pid '/tmp/e_parthenon.pid'

before_fork do |server, worker|
end

after_fork do |server, worker|
end
