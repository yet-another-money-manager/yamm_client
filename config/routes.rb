# frozen_string_literal: true

Rails.application.routes.draw do
  root to: 'app#index'
  get '/healthcheck' => 'healthcheck#index'

  if Rails.env.development? || Rails.env.test?
    get '/fake-login' => 'fake_login#index'
    post '/fake-login' => 'fake_login#create'
  end

  get '/move-to/:service' => 'app#move_to', as: 'move_to'

  api_version(module: 'V1', path: { value: 'v1' }, default: false) do
    scope '/yamm' do
      get 'expenses' => 'expenses#index'
      get 'categories' => 'categories#index'
      get 'accounts' => 'accounts#index'
    end
  end
end
